# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime


class Res_partner_heading_stg(models.Model):
	_name = 'gmb.res_partner_heading_stg'

	origin_partner_id = fields.Integer()
	partner_id = fields.Integer()
	company_name = fields.Text()
	company_id = fields.Integer()
	name = fields.Text()
	display_name = fields.Text()
	active = fields.Boolean()
	customer = fields.Boolean()
	supplier = fields.Boolean()
	comment = fields.Text()
	street = fields.Text()
	street2 = fields.Text()
	zip = fields.Text()
	city = fields.Text()
	phone = fields.Text()
	mobile = fields.Text()
	country = fields.Text()
	email = fields.Text()
	state = fields.Text()
	user_id = fields.Integer()
	user = fields.Text()
	lang = fields.Text()
	type = fields.Text()
	state_id = fields.Integer()
	country_id = fields.Integer()
	company_type = fields.Text()
	category_name = fields.Text()
	category_id = fields.Integer()
	property_product_pricelist_code = fields.Text()
	property_product_pricelist = fields.Integer()
	vat = fields.Char()
	function = fields.Char()
	website = fields.Char()
	title = fields.Char()
	opt_out = fields.Boolean()
	message_bounce = fields.Integer()
	ref = fields.Char()
	property_payment_term_code = fields.Text()
	property_payment_term_id = fields.Integer()
	trust = fields.Text()
	property_supplier_payment_term_code = fields.Text()
	property_supplier_payment_term_id = fields.Integer()
	property_account_position_code = fields.Text()
	property_account_position_id = fields.Integer()
	siret = fields.Char()
	industry_name = fields.Text()
	industry_id = fields.Integer()
	commercial_partner_code = fields.Text()
	team_code = fields.Text()
	commercial_partner_id = fields.Integer()
	team_id = fields.Integer()	
	color = fields.Integer()
	commercial_company_name = fields.Char()
	employee = fields.Boolean()
	is_company = fields.Boolean()
	credit_limit = fields.Float()
	partner_share = fields.Boolean()
	debit_limit = fields.Float()
	status = fields.Selection([('to_staging','S'), ('error','E'), ('imported','I'), ('warning','W')], default='to_staging', required=True)
	erreur = fields.Text()
	property_purchase_currency_code = fields.Text()
	property_purchase_currency_id = fields.Integer()
	property_stock_customer = fields.Integer()
	property_stock_customer_code = fields.Text()
	property_stock_supplier = fields.Integer()
	property_stock_supplier_code = fields.Text()
	property_account_receivable_code = fields.Text()
	property_account_receivable_id = fields.Integer()
	property_account_payable_code = fields.Text()
	property_account_payable_id = fields.Integer()


	@api.one
	def search_foreign_id(self):

		self.erreur = ""

		#Recherche de la compagnie par son nom pour trouver le company_id
		if self.company_name != False and self.company_name != '':
			if self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id != 0:
				self.company_id = self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id
			else:
				self.erreur = """%s - La compagnie %s n'existe pas""" % (self.erreur,self.company_name)
				self.status = 'error'

		#Recherche du country par son code pour trouver le country_id
		if self.country != False and self.country != '':
			if self.env['res.country'].search([('code', '=', self.country)], limit=1).id != 0:
				self.country_id = self.env['res.country'].search([('code', '=', self.country)], limit=1).id
			else:
				self.erreur = """%s - Le pays %s n'existe pas""" % (self.erreur,self.country)
				self.status = 'error'
		
		#Recherche du state par son code pour trouver le state_id
		if self.state != False and self.state != '':
			if self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id != 0:
				self.state_id = self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id
			else:
				self.erreur = """%s - L'Etat %s n'existe pas""" % (self.erreur,self.state)
				self.status = 'error'

		#Recherche du user par son name pour trouver le user_id
		if self.user != False and self.user != '':
			if self.env['res.users'].search([('name', '=', self.user)], limit=1).id != 0:
				self.user_id = self.env['res.users'].search([('name', '=', self.user)], limit=1).id
			else:
				self.erreur = """%s - Le vendeur %s n'existe pas""" % (self.erreur,self.user)
				self.status = 'error'

		#Recherche de la Conditions de Paiement Client par son name pour trouver le property_payment_term_id
		if self.property_payment_term_code != False and self.property_payment_term_code != '':
			if self.env['account.payment.term'].search([('name', '=', self.property_payment_term_code)], limit=1).id != 0:
				self.property_payment_term_id = self.env['account.payment.term'].search([('name', '=', self.property_payment_term_code)], limit=1).id
			else:
				self.erreur = """%s - La Conditions de Paiement Client %s n'existe pas""" % (self.erreur,self.property_payment_term_code)
				self.status = 'error'

		#Recherche de la Conditions de Paiement du Vendeur par son name pour trouver le property_supplier_payment_term_id
		if self.property_supplier_payment_term_code != False and self.property_supplier_payment_term_code != '':
			if self.env['account.payment.term'].search([('name', '=', self.property_supplier_payment_term_code)], limit=1).id != 0:
				self.property_supplier_payment_term_id = self.env['account.payment.term'].search([('name', '=', self.property_supplier_payment_term_code)], limit=1).id
			else:
				self.erreur = """%s - La Conditions de Paiement du Vendeur %s n'existe pas""" % (self.erreur,self.property_supplier_payment_term_code)
				self.status = 'error'

		#Recherche de la Position Fiscale par son name pour trouver le property_account_position_id
		if self.property_account_position_code != False and self.property_account_position_code != '':
			if self.env['account.fiscal.position'].search([('name', '=', self.property_account_position_code)], limit=1).id != 0:
				self.property_account_position_id = self.env['account.fiscal.position'].search([('name', '=', self.property_account_position_code)], limit=1).id
			else:
				self.erreur = """%s - La Position Fiscale %s n'existe pas""" % (self.erreur,self.property_account_position_code)
				self.status = 'error'

		#Recherche de l'Industry par son name pour trouver le industry_id
		if self.industry_name != False and self.industry_name != '':
			if self.env['res.partner.industry'].search([('name', '=', self.industry_name)], limit=1).id != 0:
				self.industry_id = self.env['res.partner.industry'].search([('name', '=', self.industry_name)], limit=1).id
			else:
				self.erreur = """%s - L'Industry %s n'existe pas""" % (self.erreur,self.industry_name)
				self.status = 'error'

		#Recherche de la Liste de prix de vente par son name pour trouver la property_product_pricelist
		if self.property_product_pricelist_code != False and self.property_product_pricelist_code != '':
			if self.env['product.pricelist'].search([('name', '=', self.property_product_pricelist_code)], limit=1).id != 0:
				self.property_product_pricelist = self.env['product.pricelist'].search([('name', '=', self.property_product_pricelist_code)], limit=1).id
			else:
				self.erreur = """%s - La Liste de prix de vente %s n'existe pas""" % (self.erreur,self.property_product_pricelist_code)
				self.status = 'error'

		#Recherche de l'Étiquette par son name pour trouver le category_id
		if self.category_name != False and self.category_name != '':
			if self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id != 0:
				self.category_id = self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id
			else:
				self.erreur = """%s - L'étiquette %s n'existe pas""" % (self.erreur,self.category_name)
				self.status = 'error'

		#Recherche de l'Entité commerciale par son name pour trouver le commercial_partner_id
		if self.commercial_partner_code != False and self.commercial_partner_code != '':
			if self.env['res.partner'].search([('name', '=', self.commercial_partner_code)], limit=1).id != 0:
				self.commercial_partner_id = self.env['product.pricelist'].search([('name', '=', self.commercial_partner_code)], limit=1).id
			else:
				self.erreur = """%s - L'Entité commerciale %s n'existe pas""" % (self.erreur,self.commercial_partner_code)
				self.status = 'error'

		#Recherche de l'Équipe de vente par son name pour trouver le team_id
		if self.team_code != False and self.team_code != '':
			if self.env['crm.team'].search([('name', '=', self.team_code)], limit=1).id != 0:
				self.team_id = self.env['crm.team'].search([('name', '=', self.team_code)], limit=1).id
			else:
				self.erreur = """%s - L'Équipe de vente %s n'existe pas""" % (self.erreur,self.team_code)
				self.status = 'error'

		#Recherche de la Devise du fournisseur par son name pour trouver le property_purchase_currency_id
		if self.property_purchase_currency_code != False and self.property_purchase_currency_code != '':
			if self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id != 0:
				self.property_purchase_currency_id = self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id
			else:
				self.erreur = """%s - La Devise du fournisseur %s n'existe pas""" % (self.erreur,self.property_purchase_currency_code)
				self.status = 'error'

		#Recherche de l'Emplacement Client par son name pour trouver le property_stock_customer
		if self.property_stock_customer_code != False and self.property_stock_customer_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id != 0:
				self.property_stock_customer = self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id
			else:
				self.erreur = """%s - L'Emplacement Client %s n'existe pas""" % (self.erreur,self.property_stock_customer_code)
				self.status = 'error'

		#Recherche de l'Adresse du vendeur par son name pour trouver le property_stock_supplier
		if self.property_stock_supplier_code != False and self.property_stock_supplier_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id != 0:
				self.property_stock_supplier = self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id
			else:
				self.erreur = """%s - L'Adresse du vendeur %s n'existe pas""" % (self.erreur,self.property_stock_supplier_code)
				self.status = 'error'

		#Recherche du Compte client par son code pour trouver le property_account_receivable_id
		if self.property_account_receivable_code != False and self.property_account_receivable_code != '':
			if self.env['account.account'].search([('code', '=', self.property_account_receivable_code)], limit=1).id != 0:
				self.property_account_receivable_id = self.env['account.account'].search([('code', '=', self.property_account_receivable_code)], limit=1).id
			else:
				self.erreur = """%s - Le Compte client %s n'existe pas""" % (self.erreur,self.property_account_receivable_code)
				self.status = 'error'

		#Recherche du Compte fournisseur par son code pour trouver le property_account_payable_id
		if self.property_account_payable_code != False and self.property_account_payable_code != '':
			if self.env['account.account'].search([('code', '=', self.property_account_payable_code)], limit=1).id != 0:
				self.property_account_payable_id = self.env['account.account'].search([('code', '=', self.property_account_payable_code)], limit=1).id
			else:
				self.erreur = """%s - Le Compte fournisseur %s n'existe pas""" % (self.erreur,self.property_account_payable_code)
				self.status = 'error'

		self.create_partner()


	@api.one
	def create_partner(self):

		#ne pas creer si partner_id <> 0
		if self.partner_id == 0:

			if self.status == 'to_staging':
				client = self.env['res.partner'].create({ 
														'name': self.name, 
														'company_id' : self.company_id,
														'display_name' : self.display_name,
														'active': self.active,
														'customer' : self.customer,
														'supplier' : self.supplier,
														'comment' : self.comment,
														'street' : self.street,
														'street2' : self.street2,
														'zip' : self.zip,
														'city' : self.city,
														'phone' : self.phone,
														'mobile' : self.mobile,
														'country_id' : self.country_id,
														'email' : self.email,
														'state_id' : self.state_id,
														'user_id' : self.user_id,
														'lang' : self.lang,
														'type' : self.type,
														'company_type' : self.company_type,
														'property_product_pricelist' : self.property_product_pricelist,
														'vat' : self.vat,
														'function' : self.function,
														'website' : self.website,
														'title' : self.title,
														'opt_out' : self.opt_out,
														'message_bounce' : self.message_bounce,
														'ref' : self.ref,
														'property_payment_term_id' : self.property_payment_term_id,
														'trust' : self.trust,
														'property_supplier_payment_term_id' : self.property_supplier_payment_term_id,
														'property_account_position_id' : self.property_account_position_id,
														'siret' : self.siret,
														'industry_id' : self.industry_id,
														'commercial_partner_id' : self.commercial_partner_id,
														'team_id' : self.team_id,
														'color' : self.color,
														'commercial_company_name' : self.commercial_company_name,
														'employee' : self.employee,
														'is_company' : self.is_company,
														'credit_limit' : self.credit_limit,
														'partner_share' : self.partner_share,
														'debit_limit' : self.debit_limit,
														'property_purchase_currency_id' : self.property_purchase_currency_id,
														'property_stock_customer' : self.property_stock_customer,
														'property_stock_supplier' : self.property_stock_supplier,
														'property_account_receivable_id' : self.property_account_receivable_id,
														'property_account_payable_id' : self.property_account_payable_id,
														})
				if client is not None:	
					self.partner_id = client.id
					self.create_category(client)
					self.status = 'imported'
					self.create_childs()
				else:
					self.status = 'error'
					self.erreur = """%s - Impossible de créer le client %s""" % (self.erreur,self.name)
		elif self.status == 'warning':
			self.create_childs()


	@api.one
	def create_category(self,_client):
		if self.category_id != 0:
			_client.write({'category_id' : [(4, self.category_id)], })


	@api.one
	def create_childs(self):
		addresss = self.env['gmb.res_partner_address_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		contacts = self.env['gmb.res_partner_contact_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		accounts = self.env['gmb.res_partner_account_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		for adress in addresss:
			adress.search_foreign_id()
		for contact in contacts:
			contact.search_foreign_id()
		for account in accounts:
			account.search_foreign_id()


	@api.one
	def update_status(self):
		addresss = self.env['gmb.res_partner_address_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		contacts = self.env['gmb.res_partner_contact_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		accounts = self.env['gmb.res_partner_account_stg'].search([('origin_partner_id', '=', self.origin_partner_id)])
		for adress in addresss:
			if adress.status == 'imported':
				self.status = 'imported'
			elif adress.status == 'error':
				self.status = 'warning'
				return True
		for contact in contacts:
			if contact.status == 'imported':
				self.status = 'imported'
			elif contact.status == 'error':
				self.status = 'warning'
				return True
		for account in accounts:
			if account.status == 'imported':
				self.status = 'imported'
			elif account.status == 'error':
				self.status = 'warning'
				return True
		return True



class Res_partner_address_stg(models.Model):
	_name = 'gmb.res_partner_address_stg'

	origin_partner_id = fields.Integer()
	partner_id = fields.Integer()
	parent_id = fields.Integer()
	name = fields.Text()
	display_name = fields.Text()
	active = fields.Boolean()	
	comment = fields.Text()
	street = fields.Text()
	street2 = fields.Text()
	zip = fields.Text()
	city = fields.Text()
	phone = fields.Text()
	mobile = fields.Text()
	country = fields.Text()
	country_id = fields.Integer()
	state = fields.Text()
	state_id = fields.Integer()
	email = fields.Text()
	type = fields.Text()
	status = fields.Selection([('to_staging','S'), ('error','E'), ('imported','I')], default='to_staging', required=True)
	erreur = fields.Text()
	company_name = fields.Text()
	company_id = fields.Integer()
	lang = fields.Text()
	vat = fields.Char()
	category_name = fields.Text()
	category_id = fields.Integer()
	function = fields.Char()
	website = fields.Char()
	title = fields.Char()
	customer = fields.Boolean()
	supplier = fields.Boolean()
	user_id = fields.Integer()
	user = fields.Text()
	property_purchase_currency_code = fields.Text()
	property_purchase_currency_id = fields.Integer()
	ref = fields.Char()
	opt_out = fields.Boolean()
	message_bounce = fields.Integer()
	property_stock_customer = fields.Integer()
	property_stock_customer_code = fields.Text()
	property_stock_supplier = fields.Integer()
	property_stock_supplier_code = fields.Text()
	employee = fields.Boolean()
	is_company = fields.Boolean()


	@api.one
	def search_foreign_id(self):

		self.erreur = ""

		#Recherche du country par son code pour trouver le country_id
		if self.country != False and self.country != '':
			if self.env['res.country'].search([('code', '=', self.country)], limit=1).id != 0:
				self.country_id = self.env['res.country'].search([('code', '=', self.country)], limit=1).id
			else:
				self.erreur = """%s - Le pays %s n'existe pas""" % (self.erreur,self.country)
				self.status = 'error'
		
		#Recherche du state par son code pour trouver le state_id
		if self.state != False and self.state != '':
			if self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id != 0:
				self.state_id = self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id
			else:
				self.erreur = """%s - L'Etat %s n'existe pas""" % (self.erreur,self.state)
				self.status = 'error'

		#Recherche de la compagnie par son nom pour trouver le company_id
		if self.company_name != False and self.company_name != '':
			if self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id != 0:
				self.company_id = self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id
			else:
				self.erreur = """%s - La compagnie %s n'existe pas""" % (self.erreur,self.company_name)
				self.status = 'error'

		#Recherche de l'Étiquette par son name pour trouver le category_id
		if self.category_name != False and self.category_name != '':
			if self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id != 0:
				self.category_id = self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id
			else:
				self.erreur = """%s - L'étiquette %s n'existe pas""" % (self.erreur,self.category_name)
				self.status = 'error'

		#Recherche du user par son name pour trouver le user_id
		if self.user != False and self.user != '':
			if self.env['res.users'].search([('name', '=', self.user)], limit=1).id != 0:
				self.user_id = self.env['res.users'].search([('name', '=', self.user)], limit=1).id
			else:
				self.erreur = """%s - Le vendeur %s n'existe pas""" % (self.erreur,self.user)
				self.status = 'error'

		#Recherche de la Devise du fournisseur par son name pour trouver le property_purchase_currency_id
		if self.property_purchase_currency_code != False and self.property_purchase_currency_code != '':
			if self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id != 0:
				self.property_purchase_currency_id = self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id
			else:
				self.erreur = """%s - La Devise du fournisseur %s n'existe pas""" % (self.erreur,self.property_purchase_currency_code)
				self.status = 'error'

		#Recherche de l'Emplacement Client par son name pour trouver le property_stock_customer
		if self.property_stock_customer_code != False and self.property_stock_customer_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id != 0:
				self.property_stock_customer = self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id
			else:
				self.erreur = """%s - L'Emplacement Client %s n'existe pas""" % (self.erreur,self.property_stock_customer_code)
				self.status = 'error'

		#Recherche de l'Adresse du vendeur par son name pour trouver le property_stock_supplier
		if self.property_stock_supplier_code != False and self.property_stock_supplier_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id != 0:
				self.property_stock_supplier = self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id
			else:
				self.erreur = """%s - L'Adresse du vendeur %s n'existe pas""" % (self.erreur,self.property_stock_supplier_code)
				self.status = 'error'

		#On vérifie que le titre (civilité) existe
		if self.title != False and self.title != '':
			if self.env['res.partner.title'].search([('name', '=', self.title)], limit=1).id == 0:
				self.erreur = """%s - Le titre %s n'existe pas""" % (self.erreur,self.title)
				self.status = 'error'

		self.search_parent()


	@api.one
	def search_parent(self):

		#Recherche du partner_id par son origin_partner_id pour trouver le parent_id
		if self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id != 0:
			self.parent_id = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id
			self.create_adresse_partner()
		else:
			self.erreur = """%s - Le client parent %s n'existe pas""" % (self.erreur,self.origin_partner_id)
			self.status = 'error'


	@api.one
	def create_adresse_partner(self):

		if self.partner_id == 0:

			#On récupére le client parent
			_client = self.env['res.partner'].search([('id', '=', self.parent_id)], limit=1)

			if self.status == 'to_staging':
				client = self.env['res.partner'].create({ 
														'name': self.name, 
														'parent_id' : self.parent_id,
														'display_name' : self.display_name,
														'active': self.active, 
														'comment' : self.comment,
														'street' : self.street,
														'street2' : self.street2,
														'zip' : self.zip,
														'city' : self.city,
														'phone' : self.phone,
														'mobile' : self.mobile,
														'country_id' : self.country_id,
														'email' : self.email,
														'state_id' : self.state_id,
														'lang' : self.lang,
														'type' : self.type,
														'company_id' : self.company_id,
														'vat' : self.vat,
														'function' : self.function,
														'website' : self.website,
														'title' : self.title,
														'customer' : self.customer,
														'supplier' : self.supplier,
														'user_id' : self.user_id,	
														'property_purchase_currency_id' : self.property_purchase_currency_id,
														'ref' : self.ref,
														'opt_out' : self.opt_out,
														'message_bounce' : self.message_bounce,
														'property_stock_customer' : self.property_stock_customer,
														'property_stock_supplier' : self.property_stock_supplier,
														'employee' : self.employee,
														'is_company' : self.is_company,
														})
				if client is not None:
					self.partner_id = client.id
					self.create_category(client)
					self.status = 'imported'
				else:
					self.status = 'error'
					self.erreur = """%s - Impossible de créer l'adresse du client %s""" % (self.erreur,_client.name)

			parent_stg = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1)
			parent_stg.update_status()


	@api.one
	def create_category(self,_client):
		if self.category_id != 0:
			_client.write({'category_id' : [(4, self.category_id)], })



class Res_partner_contact_stg(models.Model):
	_name = 'gmb.res_partner_contact_stg'

	origin_partner_id = fields.Integer()
	partner_id = fields.Integer()
	parent_id = fields.Integer()
	name = fields.Text()	
	active = fields.Boolean()	
	comment = fields.Text()
	function = fields.Text()
	phone = fields.Text()
	mobile = fields.Text()
	email = fields.Text()
	title = fields.Text()
	type = fields.Text()
	status = fields.Selection([('to_staging','S'), ('error','E'), ('imported','I')], default='to_staging', required=True)
	erreur = fields.Text()
	company_name = fields.Text()
	company_id = fields.Integer()
	lang = fields.Text()
	category_name = fields.Text()
	category_id = fields.Integer()
	website = fields.Char()
	customer = fields.Boolean()
	supplier = fields.Boolean()
	user_id = fields.Integer()
	user = fields.Text()
	property_purchase_currency_code = fields.Text()
	property_purchase_currency_id = fields.Integer()
	opt_out = fields.Boolean()
	message_bounce = fields.Integer()
	ref = fields.Char()
	property_stock_customer = fields.Integer()
	property_stock_customer_code = fields.Text()
	property_stock_supplier = fields.Integer()
	property_stock_supplier_code = fields.Text()
	employee = fields.Boolean()
	is_company = fields.Boolean()
	street = fields.Text()
	street2 = fields.Text()
	zip = fields.Text()
	city = fields.Text()
	country = fields.Text()
	country_id = fields.Integer()
	state = fields.Text()
	state_id = fields.Integer()


	@api.one
	def search_foreign_id(self):

		self.erreur = ""

		#On vérifie que le titre (civilité) existe
		if self.title != False and self.title != '':
			if self.env['res.partner.title'].search([('name', '=', self.title)], limit=1).id == 0:
				self.erreur = """%s - Le titre %s n'existe pas""" % (self.erreur,self.title)
				self.status = 'error'

		#Recherche de la compagnie par son nom pour trouver le company_id
		if self.company_name != False and self.company_name != '':
			if self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id != 0:
				self.company_id = self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id
			else:
				self.erreur = """%s - La compagnie %s n'existe pas""" % (self.erreur,self.company_name)
				self.status = 'error'

		#Recherche de l'Étiquette par son name pour trouver le category_id
		if self.category_name != False and self.category_name != '':
			if self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id != 0:
				self.category_id = self.env['res.partner.category'].search([('name', '=', self.category_name)], limit=1).id
			else:
				self.erreur = """%s - L'étiquette %s n'existe pas""" % (self.erreur,self.category_name)
				self.status = 'error'

		#Recherche du user par son name pour trouver le user_id
		if self.user != False and self.user != '':
			if self.env['res.users'].search([('name', '=', self.user)], limit=1).id != 0:
				self.user_id = self.env['res.users'].search([('name', '=', self.user)], limit=1).id
			else:
				self.erreur = """%s - Le vendeur %s n'existe pas""" % (self.erreur,self.user)
				self.status = 'error'

		#Recherche de la Devise du fournisseur par son name pour trouver le property_purchase_currency_id
		if self.property_purchase_currency_code != False and self.property_purchase_currency_code != '':
			if self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id != 0:
				self.property_purchase_currency_id = self.env['res.currency'].search([('name', '=', self.property_purchase_currency_code)], limit=1).id
			else:
				self.erreur = """%s - La Devise du fournisseur %s n'existe pas""" % (self.erreur,self.property_purchase_currency_code)
				self.status = 'error'

		#Recherche de l'Emplacement Client par son name pour trouver le property_stock_customer
		if self.property_stock_customer_code != False and self.property_stock_customer_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id != 0:
				self.property_stock_customer = self.env['stock.location'].search([('name', '=', self.property_stock_customer_code)], limit=1).id
			else:
				self.erreur = """%s - L'Emplacement Client %s n'existe pas""" % (self.erreur,self.property_stock_customer_code)
				self.status = 'error'

		#Recherche de l'Adresse du vendeur par son name pour trouver le property_stock_supplier
		if self.property_stock_supplier_code != False and self.property_stock_supplier_code != '':
			if self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id != 0:
				self.property_stock_supplier = self.env['stock.location'].search([('name', '=', self.property_stock_supplier_code)], limit=1).id
			else:
				self.erreur = """%s - L'Adresse du vendeur %s n'existe pas""" % (self.erreur,self.property_stock_supplier_code)
				self.status = 'error'

		#Recherche du country par son code pour trouver le country_id
		if self.country != False and self.country != '':
			if self.env['res.country'].search([('code', '=', self.country)], limit=1).id != 0:
				self.country_id = self.env['res.country'].search([('code', '=', self.country)], limit=1).id
			else:
				self.erreur = """%s - Le pays %s n'existe pas""" % (self.erreur,self.country)
				self.status = 'error'
		
		#Recherche du state par son code pour trouver le state_id
		if self.state != False and self.state != '':
			if self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id != 0:
				self.state_id = self.env['res.country.state'].search([('code', '=', self.state)], limit=1).id
			else:
				self.erreur = """%s - L'Etat %s n'existe pas""" % (self.erreur,self.state)
				self.status = 'error'
		
		self.search_parent()


	@api.one
	def search_parent(self):

		#Recherche du partner_id par son origin_partner_id pour trouver le parent_id
		if self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id != 0:
			self.parent_id = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id
			self.create_contact_partner()
		else:
			self.erreur = """%s - Le client parent %s n'existe pas""" % (self.erreur,self.origin_partner_id)
			self.status = 'error'


	@api.one
	def create_contact_partner(self):
		
		if self.partner_id == 0:

			#On récupére le client parent
			_client = self.env['res.partner'].search([('id', '=', self.parent_id)], limit=1)

			if self.status == 'to_staging':
				client = self.env['res.partner'].create({ 
														'name': self.name, 
														'parent_id' : self.parent_id,
														'active': self.active, 
														'comment' : self.comment,
														'phone' : self.phone,
														'mobile' : self.mobile,
														'email' : self.email,
														'type' : self.type,
														'title' : self.title,
														'function' : self.function,
														'company_id' : self.company_id,
														'lang' : self.lang,
														'website' : self.website,
														'customer' : self.customer,
														'supplier' : self.supplier,
														'user_id' : self.user_id,
														'property_purchase_currency_id' : self.property_purchase_currency_id,
														'opt_out' : self.opt_out,
														'message_bounce' : self.message_bounce,
														'ref' : self.ref,
														'property_stock_customer' : self.property_stock_customer,
														'property_stock_supplier' : self.property_stock_supplier,
														'employee' : self.employee,
														'is_company' : self.is_company,
														'street' : self.street,
														'street2' : self.street2,
														'zip' : self.zip,
														'city' : self.city,
														'country_id' : self.country_id,
														'state_id' : self.state_id,
														})
				if client is not None:
					self.partner_id = client.id
					self.create_category(client)
					self.status = 'imported'
				else:
					self.status = 'error'
					self.erreur = """%s - Impossible de créer le contact du client %s""" % (self.erreur,_client.name)

			parent_stg = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1)
			parent_stg.update_status()


	@api.one
	def create_category(self,_client):
		if self.category_id != 0:
			_client.write({'category_id' : [(4, self.category_id)], })



class Res_partner_account_stg(models.Model):
	_name = 'gmb.res_partner_account_stg'

	origin_partner_id = fields.Integer()
	account_id = fields.Integer()
	parent_id = fields.Integer()
	active = fields.Boolean()
	bank = fields.Text()
	bank_id = fields.Integer()
	nom_compte = fields.Text()
	status = fields.Selection([('to_staging','S'), ('error','E'), ('imported','I')], default='to_staging', required=True)
	erreur = fields.Text()
	acc_number = fields.Text()
	currency_code = fields.Text()
	currency_id = fields.Integer()
	company_name = fields.Text()
	company_id = fields.Integer()


	@api.one
	def search_foreign_id(self):

		self.erreur = ""

		#Recherche de la banque par son nom pour trouver le bank_id
		if self.bank != False and self.bank != '':
			if self.env['res.bank'].search([('name', '=', self.bank)], limit=1).id != 0:
				self.bank_id = self.env['res.bank'].search([('name', '=', self.bank)], limit=1).id
			else:
				self.erreur = """%s - La banque %s n'existe pas""" % (self.erreur,self.bank)
				self.status = 'error'
		
		#Recherche de la compagnie par son nom pour trouver le company_id
		if self.company_name != False and self.company_name != '':
			if self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id != 0:
				self.company_id = self.env['res.company'].search([('name', '=', self.company_name)], limit=1).id
			else:
				self.erreur = """%s - La compagnie %s n'existe pas""" % (self.erreur,self.company_name)
				self.status = 'error'

		#Recherche de la Devise par son nom pour trouver le currency_id
		if self.currency_code != False and self.currency_code != '':
			if self.env['res.currency'].search([('name', '=', self.currency_code)], limit=1).id != 0:
				self.currency_id = self.env['res.currency'].search([('name', '=', self.currency_code)], limit=1).id
			else:
				self.erreur = """%s - La Devise %s n'existe pas""" % (self.erreur,self.currency_code)
				self.status = 'error'

		self.search_parent()


	@api.one
	def search_parent(self):

		#Recherche du partner_id par son origin_partner_id pour trouver le parent_id
		if self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id != 0:
			self.parent_id = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1).partner_id
			self.create_account_partner()
		else:
			self.erreur = """%s - Le client parent %s n'existe pas""" % (self.erreur,self.origin_partner_id)
			self.status = 'error'


	@api.one
	def create_account_partner(self):
		
		if self.account_id == 0:

			#On récupére le client parent
			_client = self.env['res.partner'].search([('id', '=', self.parent_id)], limit=1)

			if self.status == 'to_staging':
				account = self.env['res.partner.bank'].create({ 
														'acc_number': self.acc_number, 
														'partner_id' : self.parent_id,
														'active': self.active, 
														'bank_id': self.bank_id,
														'currency_id' : self.currency_id,
														'company_id' : self.company_id,
														})
				if account is not None:
					self.account_id = account.id
					self.status = 'imported'
				else:
					self.status = 'error'
					self.erreur = """%s - Impossible de créer le compte du client %s""" % (self.erreur,_client.nom)

			parent_stg = self.env['gmb.res_partner_heading_stg'].search([('origin_partner_id', '=', self.origin_partner_id)], limit=1)
			parent_stg.update_status()