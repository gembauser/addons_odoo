# -*- coding: utf-8 -*-
{
    'name': "GMB Modèles staging",
    'summary': """Modèles staging""",
    'description': """
        - Ajout des modèles staging et de leurs fonctions pour le transfert de données entre Gemba et Odoo
    """,
    'author': "Gembaware",
    'website': "http://www.gembaware.com/",
    'category': 'GMB',
    'version': '1.0',
    'depends': ['base','sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/staging_table.xml',
        #'views/tree_view_asset.xml',
    ],
    #'qweb': ['static/src/xml/project_button.xml',],


    'demo': [
    ],
}